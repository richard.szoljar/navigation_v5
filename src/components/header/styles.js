import { StyleSheet } from 'react-native';

import { COLORS } from '../../theme';

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.primary,
  },
});

export { styles as Header };
