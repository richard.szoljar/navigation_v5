import React from 'react';
import { Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Icon = ({ source, color, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <Image source={source} style={{ tintColor: color }} />
  </TouchableOpacity>
);

export { Icon };
