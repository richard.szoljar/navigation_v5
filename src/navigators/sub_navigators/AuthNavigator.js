import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import { LoginScreen, RegisterScreen } from '../../screens';
import { Header, Icon } from '../../components';
import { IMAGES } from '../../assets';

const Stack = createStackNavigator();

const AuthNavigator = () => {
  useEffect(() => {
    StatusBar.setBarStyle('light-content');
  }, []);

  return (
    <Stack.Navigator
      screenOptions={{
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerStyle: Header.container,
      }}
    >
      <Stack.Screen
        name='LoginScreen'
        component={LoginScreen}
        options={({ navigation }) => ({
          title: 'Login',
          headerRight: ({ tintColor }) => (
            <Icon
              onPress={() => navigation.navigate('RegisterScreen')}
              source={IMAGES.ADD_PERSON}
              color={tintColor}
            />
          ),
          headerRightContainerStyle: { marginRight: 16 },
        })}
      />
      <Stack.Screen
        name='RegisterScreen'
        component={RegisterScreen}
        options={{ title: 'Register' }}
      />
    </Stack.Navigator>
  );
};

export { AuthNavigator };
