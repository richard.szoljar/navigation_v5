import React from 'react';
import { Text, Button } from 'react-native';

import { Screen } from '../../components';

const RegisterScreen = ({ navigation }) => {
  return (
    <Screen>
      <Text>RegisterScreen</Text>
      <Button title='Back to login' onPress={() => navigation.goBack()} />
    </Screen>
  );
};

export { RegisterScreen };
