import React from 'react';
import { Image } from 'react-native';

import { Screen } from '../../components';
import { IMAGES } from '../../assets';
import { styles } from './styles';

const SplashScreen = () => {
  return (
    <Screen style={styles.container}>
      <Image source={IMAGES.SPLASH_SCREEN} resizeMode='contain' style={styles.image} />
    </Screen>
  );
};

export { SplashScreen };
