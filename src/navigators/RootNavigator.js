import React, { useContext } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { AuthNavigator, MainNavigator } from './sub_navigators';
import { AuthContext } from '../contexts';
import { SplashScreen } from '../screens';

const Stack = createStackNavigator();

const RootNavigator = () => {
  const { user, loading } = useContext(AuthContext);

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {loading && <Stack.Screen name='SplashScreen' component={SplashScreen} />}
        {user ? (
          <Stack.Screen name='MainNavigator' component={MainNavigator} />
        ) : (
          <Stack.Screen name='AuthNavigator' component={AuthNavigator} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export { RootNavigator };
