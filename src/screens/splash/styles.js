import { StyleSheet } from 'react-native';
import { COLORS } from '../../theme';

export const styles = StyleSheet.create({
  container: {
    padding: 16,
    backgroundColor: COLORS.primary,
  },
  image: {
    width: '100%',
  },
});
