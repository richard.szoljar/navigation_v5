import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 16,
    justifyContent: 'space-between',
  },
  input: {
    borderWidth: 1,
    textAlign: 'left',
    alignSelf: 'stretch',
    padding: 8,
    marginBottom: 16,
  },
  topContainer: {
    alignSelf: 'stretch',
  },
});
