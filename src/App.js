import React from 'react';
import { StatusBar } from 'react-native';

import { AuthProvider } from './contexts';
import { RootNavigator } from './navigators';
import { COLORS } from './theme';

const App = () => {
  return (
    <AuthProvider>
      <StatusBar barStyle='light-content' backgroundColor={COLORS.primary} />
      <RootNavigator />
    </AuthProvider>
  );
};

export default App;
