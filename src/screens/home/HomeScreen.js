import React, { useContext, useCallback } from 'react';
import { Text, Button, BackHandler, Alert } from 'react-native';

import { useFocusEffect } from '@react-navigation/native';
import { Screen } from '../../components';
import { AuthContext } from '../../contexts';

const HomeScreen = () => {
  const { logout } = useContext(AuthContext);

  useFocusEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onHardwareBackPress);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onHardwareBackPress);
    };
  }, [onHardwareBackPress]);

  const onHardwareBackPress = useCallback(() => {
    Alert.alert('Kilépés', 'Biztosan ki akar lépni?', [
      { text: 'Nem' },
      { text: 'Igen', onPress: () => BackHandler.exitApp() },
    ]);
    return true;
  }, []);

  return (
    <Screen>
      <Text>HomeScreen</Text>
      <Button title='Logout' onPress={logout} />
    </Screen>
  );
};

export { HomeScreen };
