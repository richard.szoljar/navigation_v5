import React, { useEffect } from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { StatusBar, Platform } from 'react-native';
import { HomeScreen, ProfileScreen, SettingsScreen } from '../../screens';
import { IMAGES } from '../../assets';
import { Icon } from '../../components';
import { COLORS } from '../../theme';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  useEffect(() => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content');
    }
  }, []);

  return (
    <Tab.Navigator
      tabBarOptions={{ activeTintColor: COLORS.primary, inactiveTintColor: COLORS.secondary }}
      backBehavior='initialRoute'
    >
      <Tab.Screen
        name='HomeScreen'
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => <Icon source={IMAGES.HOME} color={color} />,
        }}
      />
      <Tab.Screen
        name='ProfileScreen'
        component={ProfileScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color }) => <Icon source={IMAGES.PERSON} color={color} />,
        }}
      />
      <Tab.Screen
        name='SettingsScreen'
        component={SettingsScreen}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({ color }) => <Icon source={IMAGES.SETTINGS} color={color} />,
        }}
      />
    </Tab.Navigator>
  );
};

export { MainNavigator };
