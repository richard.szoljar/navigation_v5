import React, { useContext } from 'react';
import { Text, Button } from 'react-native';

import { Screen } from '../../components';
import { AuthContext } from '../../contexts';

const ProfileScreen = () => {
  const { logout, user } = useContext(AuthContext);

  return (
    <Screen>
      <Text>ProfileScreen</Text>
      <Text>{`Hello ${user?.name}`}</Text>
      <Button title='Logout' onPress={logout} />
    </Screen>
  );
};

export { ProfileScreen };
