import React, { useContext } from 'react';
import { Text, Button } from 'react-native';

import { Screen } from '../../components';
import { AuthContext } from '../../contexts';

const SettingsScreen = () => {
  const { logout } = useContext(AuthContext);

  return (
    <Screen>
      <Text>SettingsScreen</Text>
      <Button title='Logout' onPress={logout} />
    </Screen>
  );
};

export { SettingsScreen };
