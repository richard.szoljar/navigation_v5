import React, { useState, useCallback, useEffect } from 'react';

import AsyncStorage from '@react-native-community/async-storage';

export const AuthContext = React.createContext({
  user: null,
  loading: true,
  login: () => {},
  logout: () => {},
});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getUser();
  }, [getUser]);

  const getUser = useCallback(async () => {
    const response = await AsyncStorage.getItem('user');
    const storedUser = await JSON.parse(response);

    if (storedUser) {
      setTimeout(() => {
        setLoading(false);
        login(storedUser.name, storedUser.password);
      }, 1000);
    } else {
      setTimeout(() => {
        setLoading(false);
      }, 1000);
    }
  }, [login]);

  const login = useCallback((name, password) => {
    if (!name || !password) {
      return;
    }

    const userCredentials = { name, password };
    setUser(userCredentials);
    AsyncStorage.setItem('user', JSON.stringify(userCredentials));
  }, []);

  const logout = () => {
    AsyncStorage.removeItem('user');
    setUser(null);
  };

  return (
    <AuthContext.Provider value={{ user, loading, login, logout }}>{children}</AuthContext.Provider>
  );
};
