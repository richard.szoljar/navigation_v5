export * from './splash';
export * from './login';
export * from './register';
export * from './home';
export * from './profile';
export * from './settings';
