export const IMAGES = {
  SPLASH_SCREEN: require('./images/splash-screen.png'),
  ADD_PERSON: require('./images/add_person.png'),
  PERSON: require('./images/person.png'),
  SETTINGS: require('./images/settings.png'),
  HOME: require('./images/home.png'),
};
