import React, { useContext, useState } from 'react';
import { View, Button, TextInput } from 'react-native';

import { Screen } from '../../components';
import { AuthContext } from '../../contexts';
import { styles } from './styles';

const LoginScreen = ({ navigation }) => {
  const { login } = useContext(AuthContext);
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Screen style={styles.container}>
      <View style={styles.topContainer}>
        <TextInput
          value={name}
          onChangeText={(text) => setName(text)}
          placeholder='Name'
          style={styles.input}
        />
        <TextInput
          value={password}
          onChangeText={(text) => setPassword(text)}
          placeholder='Password'
          style={styles.input}
          secureTextEntry
        />
        <Button title='Login' onPress={() => login(name, password)} />
      </View>
      <Button title='Go to register' onPress={() => navigation.navigate('RegisterScreen')} />
    </Screen>
  );
};

export { LoginScreen };
